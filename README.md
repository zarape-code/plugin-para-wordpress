# Plugin para wordpress con Vuejs

Crear un plugin para wordpress utilizando vue js para las funcionalidades


## Estructura del proyecto
```sh
\  
  |-zarape-code-plugin.php    # Archivo inicial con la configuración del plugin.
  |-includes
    |-functions.php           # Incluye las funciones que ejecutará el plugin y la declaración de la configuración del admin
    |-admin.php               # Información de la vista principal del plugin
    |-mi-plugin.php           # shortcode del plugin
    |-option_2.php            # Ejemplo de una pagina de un submenú del administrador
```


## Tecnología empleada
* php
* [Vue.js](https://es.vuejs.org/v2/guide/installation.html#CDN) - CDN Web Framework  


___
### Autor
Creado por [Ulises Uribe](https://www.linkedin.com/in/ulises-uribe/) para zarape.io 🇲🇽
