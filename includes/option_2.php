<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.9.2/css/bulma.min.css">
<script src="https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js"></script>
<div  id="app"  class="wrap">
    <h2>Opción 2</h2>
    <p>{{message}}</p>
    <p><button class="button" @click="aumentar">Presioname</button> {{contador}}</p>
    </div>
</div>
<script>
var app = new Vue({
  el: '#app',
  data: {
    message: 'Soy la vista de la opción 2',
    contador: 1
  },
  methods: {
    aumentar () {
        this.contador++
    }   
  }
})
</script>