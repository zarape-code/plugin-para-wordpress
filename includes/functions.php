<?php
function add_menu_administrador(){
    add_menu_page('Plugin Zarape Code','Plugin Zarape Code','manage_options','zarape-io-admin','add_logic_admin');
    add_submenu_page('zarape-io-admin', 'Opción 1','Opción 1','manage_options','opcion-1','option_1');
    add_submenu_page('zarape-io-admin', 'Opción 2','Opción 2','manage_options','opcion-2','option_2');
}
add_action('admin_menu','add_menu_administrador');

define( 'ZARAPE_CODE_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );

require_once( ZARAPE_CODE_PLUGIN_DIR . 'mi-plugin.php' );

function add_logic_admin(){
  include_once(ZARAPE_CODE_PLUGIN_DIR. 'admin.php');
}

function option_1(){
  echo "Soy una página interna.";
}

function option_2(){
  include_once(ZARAPE_CODE_PLUGIN_DIR. 'option_2.php');
}

?>